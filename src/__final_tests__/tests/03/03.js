// IN THIS TEST:
//
// IN THE PREVIOUS TWO EXERCISES WE MADE 2 TESTS PASS, BUT YOU WILL NOTICE WE'RE DOING THE EXACT
// SAME TESTS THROUGH MULTIPLE FILES.
// 1) INSTEAD OF HAVING MULTIPLE OF THE SAME TESTS, WE CAN MAKE OUR LIVES EASIER BY WRITING MORE
// GENERIC, REUSABLE TESTS.
// 2) ALSO FINISH THE TESTS BY TESTING THE INPUTS IN THE SAME WAY
//
//
// YOU MUST UNCOMMENT THE TESTING CODE
//
// __FINAL_TESTS__ / TESTS / 03.JS - Has the final solution if you get stuck

// form testing with react-testing-library
import React from 'react'
import {cleanup, render} from 'react-testing-library'
import 'jest-styled-components'

// our helpers to test
import {PRIMARY_COLOR, SECONDARY_COLOR, BORDER_RADIUS, PADDING} from '../../components/03'
import {Button, Input} from '../../components/03'

afterEach(cleanup)

// Our generic test
const checkElementForStyling = Component => {
  test('Test that the elements have the correct styling', () => {
    const {container} = render(<Component />)
    const buttons = container.querySelectorAll('button')
    const inputs = container.querySelectorAll('inputs')

    if (buttons && buttons.length) {
      buttons.forEach(element => {
        expect(element).toHaveStyleRule('background', PRIMARY_COLOR)
        expect(element).toHaveStyleRule('color', SECONDARY_COLOR)
        expect(element).toHaveStyleRule('border-radius', BORDER_RADIUS)
        expect(element).toHaveStyleRule('padding', PADDING)
      })
    }

    if (inputs && inputs.length) {
      inputs.forEach(element => {
        expect(element).toHaveStyleRule('border', `1px solid ${PRIMARY_COLOR}`)
        expect(element).toHaveStyleRule('color', PRIMARY_COLOR)
        expect(element).toHaveStyleRule('border-radius', BORDER_RADIUS)
        expect(element).toHaveStyleRule('padding', PADDING)
      })
    }
  })
}

// Our test cases
checkElementForStyling(Button)
checkElementForStyling(Input)
