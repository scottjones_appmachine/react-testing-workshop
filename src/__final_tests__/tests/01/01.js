// form testing with react-testing-library
import React from 'react'
import {cleanup} from 'react-testing-library'

// our helpers to test
import {multiplyByTwo, dividedByTwo, addToItself} from '../helpers'

afterEach(cleanup)

test('Test all utilities seperatelty', () => {
  expect(multiplyByTwo(2)).toBe(4)
  expect(dividedByTwo(8)).toBe(4)
  expect(addToItself(12)).toBe(24)
})

test('Combine the tests on a single variable', () => {
  let int = 1

  // test the multiply
  int = multiplyByTwo(int)
  expect(int).toBe(2)

  // Lets multiply it again
  int = multiplyByTwo(int)
  expect(int).toBe(4)

  // Lets divide it
  int = dividedByTwo(int)
  expect(int).toBe(2)

  // Lets add it too itself
  int = addToItself(int)
  expect(int).toBe(4)
})
