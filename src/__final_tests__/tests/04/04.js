// IN THIS TEST:
//
// UNTIL NOW ALL WE HAVE DONE IS UNIT TESTING, TESTING SMALL ISOLATED PIECES OF CODE.
// WE WILL NOW START SOME INTERGRATION TESTS BY TESTING MULTIPLE COMPONENTS.
//
// 1) FINISH WRITING THE TEST, BY TOGGLING THE BUTTON A FEW MORE TIMES
// 2) SEE HOW YOU CAN CLEANUP THE TEST TO MAKE IT MORE REUSABLE AND CLEAN
//
//
// YOU MUST UNCOMMENT THE TESTING CODE
//
// __FINAL_TESTS__ / TESTS / 04.JS - Has the final solution if you get stuck

// form testing with react-testing-library
import React from 'react'
import {cleanup, render, fireEvent} from 'react-testing-library'
import 'jest-styled-components'

// our helpers to test
import Toggle from '../../components/04'

afterEach(cleanup)

test('Test that the elements have the correct styling', () => {
  const {container} = render(<Toggle onToggle={() => {}}>
    <Toggle.On> <div className='state'>The button is on</div> </Toggle.On>
    <Toggle.Off> <div className='state'>The button is off</div> </Toggle.Off>
    <Toggle.Button />
  </Toggle>)
  const getElement = selector => container.querySelector(selector)

  expect(getElement('.state').textContent).toBe('The button is off')
  fireEvent.click(getElement('button'))
  expect(getElement('.state').textContent).toBe('The button is on')
})
