// Component dependencies
import React, {useState} from 'react'
import styled from 'styled-components'

// Our styled component
const StyledComponent = styled.div`
  display: block;
  padding: 20px;
`

// Our component logic
export const multiplyByTwo = int => {
  return int * 2
}

export const dividedByTwo = int => {
  return int / 2
}

export const addToItself = int => {
  return int + int
}

// Our presentational component
const ComponentWrapper = (props) => {
  const [state, setState] = useState({value: 0})
  const handleClick = e => {
    const actionType = e.target.dataset.type
    const actions = {
      'plus': addToItself,
      'multiply': multiplyByTwo,
      'divied': dividedByTwo
    }

    setState({value: actions[actionType](state)})
  }
  return <StyledComponent>
    <button data-type='plus' onClick={handleClick} />
    <button data-type='multiply' onClick={handleClick} />
    <button data-type='divied' onClick={handleClick} />

    <div> {state.value} </div>
  </StyledComponent>
}
export default ComponentWrapper
