export const multiplyByTwo = int => {
  return int * 2
}

export const dividedByTwo = int => {
  return int / 2
}

export const addToItself = int => {
  return int + int
}
