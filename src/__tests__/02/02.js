// IN THIS TEST:
//
// YOU NEED TO SLIGHTLY CHANGE THE COMPONENT IMPORTED BELOW, SO THAT THE TESTS BELOW PASS.
// YOU CANNOT CHANGE THE TEST, OR THE IMPORT OF THE FUNCTIONS.
//
//
// YOU MUST UNCOMMENT THE TESTING CODE
//
// __FINAL_TESTS__ / COMPONENTS / 02.JS - Has the final solution if you get stuck

// form testing with react-testing-library
import React from 'react'
import {cleanup} from 'react-testing-library'

// our helpers to test
import {multiplyByTwo, dividedByTwo, addToItself} from '../../components/02'

afterEach(cleanup)

test('Make a component more unit-testable', () => {
  // let int = 1
  //
  // // test the multiply
  // int = multiplyByTwo(int)
  // expect(int).toBe(2)
  //
  // // Lets multiply it again
  // int = multiplyByTwo(int)
  // expect(int).toBe(4)
  //
  // // Lets divide it
  // int = dividedByTwo(int)
  // expect(int).toBe(2)
  //
  // // Lets add it too itself
  // int = addToItself(int)
  // expect(int).toBe(4)
})
