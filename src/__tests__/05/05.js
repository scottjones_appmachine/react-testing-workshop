// IN THIS TEST:
//
// USE EVERYTHING YOU HAVE SEEN FROM THE PREVIOUS EXERCISES TO TEST THE FOLLOWING:
//
// * INPUTS HAVE THE CORRECT STYLING
// * INPUTS HAVE THE CORRECT DATA ATTRIBUTES
// * THAT YOU CAN USE THE COMPONENT
//
// YOU MUST UNCOMMENT THE TESTING CODE
//
// __FINAL_TESTS__ / TESTS / 05.JS - Has the final solution if you get stuck

// form testing with react-testing-library
import React from 'react'
import {cleanup, render, fireEvent} from 'react-testing-library'
import 'jest-styled-components'

// our helpers to test
import {Component} from '../../components/05/05'
import {ViewComponent} from '../../components/05/05-view'
import {testColors, testDataElements} from './helpers'

afterEach(cleanup)

testColors(ViewComponent)
testDataElements(ViewComponent)

test('Test that the elements have the correct styling and attributes', () => {
  const {container} = render(ViewComponent)
  const view = container.querySelector('.view')

  testColors(view)
  testDataElements(view)
})

test('Testing the component logic', () => {
  const {container} = render(Component)
  // ToDo
})
