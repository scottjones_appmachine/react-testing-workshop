// form testing with react-testing-library
import React from 'react'
import {cleanup, render, fireEvent} from 'react-testing-library'
import 'jest-styled-components'

import {PRIMARY_COLOR, SECONDARY_COLOR, BORDER_RADIUS, PADDING} from '../../components/05/05-view'

afterEach(cleanup)

export const testColors = dom => {
  if (!dom || !dom.querySelectorAll) {
    return
  }

  const buttons = dom.querySelectorAll('button')
  const inputs = dom.querySelectorAll('input')

  test('Testing that the components have the correct styling', () => {
    if (buttons && buttons.length) {
      buttons.forEach(element => {
        expect(element).toHaveStyleRule('background', PRIMARY_COLOR)
        expect(element).toHaveStyleRule('color', SECONDARY_COLOR)
        expect(element).toHaveStyleRule('border-radius', BORDER_RADIUS)
        expect(element).toHaveStyleRule('padding', PADDING)
      })
    }

    if (inputs && inputs.length) {
      inputs.forEach(element => {
        expect(element).toHaveStyleRule('border', `1px solid ${PRIMARY_COLOR}`)
        expect(element).toHaveStyleRule('color', PRIMARY_COLOR)
        expect(element).toHaveStyleRule('border-radius', BORDER_RADIUS)
        expect(element).toHaveStyleRule('padding', PADDING)
      })
    }
  })
}

export const testDataElements = dom => {
  if (!dom || !dom.querySelectorAll) {
    return
  }

  const buttons = dom.querySelectorAll('button')
  const inputs = dom.querySelectorAll('input')

  test('Testing that the components have the correct data', () => {
    if (inputs && inputs.length) {
      inputs.forEach(element => {
        expect(element.dataset.elementId).toBe('true')
      })
    }

    if (buttons && buttons.length) {
      buttons.forEach(element => {
        expect(element.dataset.elementId).toBe('true')
      })
    }
  })
}

test('Testing that the components have the correct styling', () => {
  // I need to remove this file for the tests
})
