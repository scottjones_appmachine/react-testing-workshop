// IN THIS TEST:
//
// FINISH WRITING TESTS FOR THE SECOND PART, USING THE FIRST TEST AS AN EXAMPLE YOU MUST:
// * WRITE A SINGLE TEST THAT USES 1 VARIABLE THAT MUST BE PASSED TO ALL 3 FUNCTIONS
// * EACH TIME YOU PASS THE VARIABLE TO THE FUNCTION YOU MUST TEST THE RESULTING VALUE
//
//
// __FINAL_TESTS__ / TESTS / 01.JS - Has the final solution if you get stuck

// form testing with react-testing-library
import React from 'react'
import {cleanup} from 'react-testing-library'

// our helpers to test
import {multiplyByTwo, dividedByTwo, addToItself} from '../../helpers'

afterEach(cleanup)

test('Test all utilities seperatelty', () => {
  expect(multiplyByTwo(2)).toBe(4)
  expect(dividedByTwo(8)).toBe(4)
  expect(addToItself(12)).toBe(24)
})

test('Change the value of a single variable by passing it through each function', () => {

})
