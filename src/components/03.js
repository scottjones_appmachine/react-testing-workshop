// Component dependencies
import React, {useState} from 'react'
import styled from 'styled-components'

// Our globalStyles
export const PRIMARY_COLOR = '#000'
export const SECONDARY_COLOR = '#fff'
export const BORDER_RADIUS = '3px'
export const PADDING = '8px'

// Our components
const ButtonComponent = styled.button`
  display: block;
  padding: ${PADDING};
  color: ${SECONDARY_COLOR};
  background: ${PRIMARY_COLOR};
  border-radius: ${BORDER_RADIUS};
`

const InputComponent = styled.input`
  display: block;
  padding: ${PADDING};
  color: ${PRIMARY_COLOR};
  border: 1px solid ${PRIMARY_COLOR};
  border-radius: ${BORDER_RADIUS};
`

// Our first presentational component
export const Button = ({children, clickHandler}) => <ButtonComponent onClick={clickHandler}> {children} </ButtonComponent>
export const Input = ({changeHandler}) => <InputComponent onChange={changeHandler} />
