// Component dependencies
import React, {useState} from 'react'
import styled from 'styled-components'

export const PRIMARY_COLOR = '#000'
export const SECONDARY_COLOR = '#fff'
export const BORDER_RADIUS = '3px'
export const PADDING = '8px'

// Our components
const ButtonComponent = styled.button`
  display: block;
  padding: ${PADDING};
  color: ${SECONDARY_COLOR};
  background: ${PRIMARY_COLOR};
  border-radius: ${BORDER_RADIUS};
`

const InputComponent = styled.input`
  display: block;
  padding: ${PADDING};
  color: ${PRIMARY_COLOR};
  border: 1px solid ${PRIMARY_COLOR};
  border-radius: ${BORDER_RADIUS};
`

const LabelComponent = styled.div`
  padding: ${PADDING};
`

// Our first presentational component
export const ViewComponent = ({data, handleClick, handleChange, state}) => {
  return <div className='view'>
    <LabelComponent> {data.inputLabel} </LabelComponent>
    <InputComponent data-elementId='loginInput' onChange={handleChange} />
    <ButtonComponent data-elementId='loginButton' onClick={handleClick}> {data.buttonLabel} </ButtonComponent>
    {
      state.showValue && <div className='value'> {state.value} </div>
    }
  </div>
}
