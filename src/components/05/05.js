// Component dependencies
import React, {useState} from 'react'
import {ViewComponent} from './05-view'

export const Component = () => {
  const [input, setInput] = useState({
    value: '',
    showValue: false
  })
  const data = {
    inputLabel: 'username',
    buttonLabel: 'login'
  }

  const handleClick = () => {
    if (input && input.length) {
      setInput({
        value: input.value,
        showValue: true
      })
    }
  }

  const handleChange = value => {
    setInput({
      value: value
    })
  }

  return <ViewComponent state={input} handleClick={handleClick} handleChange={handleChange} data={data} />
}
